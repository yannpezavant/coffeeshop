export const OurMenu =[
    {
        id: 'coffee1',
        imageUrl: '/images/14.jpg',
        title: 'Mocha',
        price: '5$'
    },
    {
        id: 'coffee2',
        imageUrl: '/images/13.jpg',
        title: 'Espresso',
        price: '7$'
    },
    {
        id: 'coffee3',
        imageUrl: '/images/3.jpg',
        title: 'Latte',
        price: '4$'
    },
    {
        id: 'coffee4',
        imageUrl: '/images/2.jpg',
        title: 'Capuccino',
        price: '6$'
    },
    {
        id: 'coffee5',
        imageUrl: '/images/12.webp',
        title: 'Frapé',
        price: '6$'
    },
]

export const Desserts=[
    {
        id:"dessert1",
        imageUrl:"/images/dessert1.jpg",
    },
    {
        id:"dessert2",
        imageUrl:"/images/dessert2.jpg",
    },
    {
        id:"dessert3",
        imageUrl:"/images/dessert3.jpg",
    },
    {
        id:"dessert4",
        imageUrl:"/images/dessert4.jpg",
    },
    {
        id:"dessert5",
        imageUrl:"/images/dessert5.jpg",
    },
    {
        id:"dessert6",
        imageUrl:"/images/dessert6.jpg",
    },
    {
        id:"dessert7",
        imageUrl:"/images/dessert7.jpg",
    },
    {
        id:"dessert8",
        imageUrl:"/images/dessert8.jpg",
    },
]