import Image from 'next/image'
import { Inter } from 'next/font/google'
import Head from 'next/head'
import Hero from '@/src/components/Layouts/Hero'
import Menu from '@/src/components/Layouts/Menu'
import Dessert from '@/src/components/Layouts/Dessert'
import About from '@/src/components/Layouts/About'
import Footer from '@/src/components/Layouts/Footer'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <>
    <Head>
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      <meta name="description" content="fresh coffee" />
      <link rel="icon" href="/favicon.ico" />
      <title>Fresh Coffee</title>
    </Head>

    
    {/*Home*/}
    <section id="home">
      <Hero/>
    </section>

    <section id="menu">
      <Menu/>
    </section>

    <section id="dessert">
      <Dessert/>
    </section>

    <section id="about">
      <About/>
    </section>

    <section id="footer">
      <Footer/>
    </section>
    

    </>
  )
}
