import React from 'react'
import { useState } from 'react'
import { Section } from './Section'
import { RunningText, TitlePage } from './TypingText'
import { OurMenu } from '@/constant'
import { CoffeeCard } from './CoffeeCard'


type Props = {}

export default function Menu({}: Props) {
    const [active, setActive] = useState('coffee3');

  return (
    <Section id='menu'>
        <RunningText />
        <TitlePage title="Our Main Menus"/>

        <div className='mt-[50px] flex flex-row min-h-[70vh] gap-2'>
            {OurMenu.map((coffee, index) => (
                <CoffeeCard key={coffee.id} {...coffee} active={active} handleClick={setActive}/>
            ))}

        </div>
    </Section>
  )
}

