import React from 'react'
import { Section } from './Section'
import { RunningText, TitlePage } from './TypingText'
import { Desserts } from '@/constant'
import { DessertCard } from './DessertCard'


const Dessert = () => {
  return (
    <Section id="dessert">
        <RunningText />
        <TitlePage title="Desserts"/>
        <div className='mt-[50px] min-h-[70vh] grid grid-cols-4 gap-[2px] mb-16'>
            {Desserts.map((item, index) => (
                <DessertCard key={item.id} {...item} />
            ))}
        </div>

    </Section>
  )
}

export default Dessert