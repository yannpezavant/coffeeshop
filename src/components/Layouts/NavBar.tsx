import React from 'react'
import Image from 'next/image'
import Link from 'next/link'


type Props = {}

export default function NavBar({}: Props) {
  return (
    <div className='px-[8rem] py-[1rem] relative z-[10]'>
        <div className='flex justify-between items-center'>
            <div className=''>
                <Image src='/images/result.png' width={70} height={70} alt='logo'/>
            </div>

            <ul className='flex gap-4 text-lg'>
                <li>
                    <Link href='/' scroll={false}>Home</Link>
                </li>
                <li>
                    <Link href='#menu' scroll={false}>Menu</Link>
                </li>
                <li>
                    <Link href='#dessert' scroll={false}>Dessert</Link>
                </li>
                <li>
                    <Link href='#about' scroll={false}>About us</Link>
                </li>
                <li>
                    <Link href='#contact' scroll={false}>Contact </Link>
                </li>
            </ul>
        </div>

    </div>
  )
}
