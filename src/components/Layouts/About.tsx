import React from 'react'
import { TitlePage, RunningText } from './TypingText'
import { Section } from './Section'
import Image from 'next/image'

type Props = {}

function About({ }: Props) {
    return (
        <Section id='about'>

            <div className='h-screen grid grid-cols-2'>
                <div className='flex justify-center items-center'>
                    <Image
                        src="/images/6.jpg"
                        alt='coffee-shop'
                        width={450}
                        height={600}
                        className='rounded-xl'
                    />
                </div>

                <div className='absolute top-0 right-0 w-1/2 h-full bg-[#333131] z-0 '/>
                    <div className='z-50 py-8 px-10  relative justify-start items-center flex'>
                        <h1 className='absolute  top-10 left-10  text-6xl font-bold  text-white opacity-10'>freshcoffee
                        </h1>
                        <div>
                            <RunningText
                                color='brown'
                                align='left'
                            />
                            <TitlePage
                                title='about us'
                                color='white'
                                align='left'
                            />
                            <div className='mt-8 text-[#e8e8e8] text-lg font-regular text-justify'>
                                <p className='opacity-90'>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dicta, veritatis exercitationem eos, obcaecati sapiente recusandae consequuntur in nihil perspiciatis consectetur libero officia dolores inventore quaerat minima, ipsam architecto cum quibusdam.
                                    Facilis, itaque. Laudantium ipsa, accusantium voluptate sunt quisquam quasi optio, quae ullam velit ea dolores sed inventore, dolorem labore eius assumenda voluptatem quod mollitia doloremque aliquid! Natus sunt atque non!

                                </p>

                            </div>
                        </div>
                    </div>
            </div>

        </Section>


    )
}

export default About