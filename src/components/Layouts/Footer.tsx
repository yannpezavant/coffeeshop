import React from 'react'
import { HiOutlineMail, HiOutlineLocationMarker } from 'react-icons/hi'
import { FooterTitle } from './FooterTitle'


type Props = {}

function Footer({ }: Props) {
    const date = new Date();
    return (
        <section id="contact" className='scroll-smooth h-[50vh] w-screen overflow-x-hidden relative px-[8rem] py-[1rem] bg-[#333131] items-center'>
            <div className='grid grid-cols-2 mt-12'>
                <div>
                    <FooterTitle>Contact</FooterTitle>
                    <div className='flex items-center text-[#e8e8e8] opacity-90'>
                        <HiOutlineMail />
                        <p className='ml-2'>freshcoffee@gmail.com </p>
                        </div>
                        <div className='flex items-center text-[#e8e8e8] opacity-90'>
                            <HiOutlineLocationMarker />
                            <p className='ml-2'>1862 Peck Street - New Hampshire - 03060 </p>
                        </div>
                    </div>

                    <div className='flex justify-around'>
                        <div className='flex flex-col'>
                            <FooterTitle>Social Media</FooterTitle>
                            <a href="" className='text-[#e8e8e8] opacity-90'>Instagram</a>
                            <a href="" className='text-[#e8e8e8] opacity-90'>Whatsapp</a>
                            <a href="" className='text-[#e8e8e8] opacity-90'>Facebook</a>
                            <a href="" className='text-[#e8e8e8] opacity-90'>TikTok</a>
                        </div>
                        <div>
                            <FooterTitle>Product</FooterTitle>
                            <p className='text-[#e8e8e8] opacity-90'>Coffee</p>
                            <p className='text-[#e8e8e8] opacity-90'>Desserts</p>
                            <p className='text-[#e8e8e8] opacity-90'>Beverages</p>
                        </div>
                        <div>
                            <FooterTitle>Facility</FooterTitle>
                            <p className='text-[#e8e8e8] opacity-90'>Wifi</p>
                            <p className='text-[#e8e8e8] opacity-90'>Comfy place</p>
                            <p className='text-[#e8e8e8] opacity-90'>Chill music</p>
                            <p className='text-[#e8e8e8] opacity-90'>Custom room</p>
                        </div>

                    </div>
                </div>

                <div className='w-full h-[1.5px] bg-[#e8e8e8] opacity-90 mt-20'>
                    <p className='text-[#e8e8e8] opacity-90 text-xs font-light tracking-wider text-center mt-2 pt-2'>&copy; {date.getFullYear()} FreshCoffee. All rights reserved </p>
                </div>

        </section>
    )
}

export default Footer