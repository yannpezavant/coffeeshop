import React from 'react'
import Image from 'next/image'



export const DessertCard=({id, imageUrl}:any)=> {
  return (
    <div className='relative h-[320px]'>
        <Image 
        src={imageUrl} 
        alt={id}
        fill
        className='object-cover rounded-xl border'
        />
            
        

    </div>
  )
}

