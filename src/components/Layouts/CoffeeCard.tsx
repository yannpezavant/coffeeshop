import { motion } from "framer-motion"
import Image from "next/image"

export const CoffeeCard = ({id, imageUrl, title, price, active, handleClick}:any) => {
    return(
        <motion.div 
        className={`${active === id ? 'flex-[10]' : 'flex-[2]'} relative flex items-center justify-center min-w-[180px] h-[450px] cursor-pointer transition-[flex] ease-in-out  duration-700 overflow-hidden`} 
        onClick={()=>handleClick(id)}>
            <Image src={imageUrl} alt='coffee' fill
            className= "rounded-xl object-cover"/>
            {active != id ? (
                <div className="absolute bottom-0 w-24 h-36 text-white text-2xl font-medium rotate-[90deg]">
                    {title}
                </div>

            ): 
            <div className="absolute p-6 w-full h-36  bottom-0 left-0 round-b-xl  bg-[rgba(0,0,0,0.5)] text-white">
                <h2 className="text-3xl font-semi-bold">{title}</h2>
                <p className="text-2xl font-medium">{price}</p>
            </div>
            }


        </motion.div>
    )
}